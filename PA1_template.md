# Reproducible Research: Peer Assessment 1
Aryan Lowell C. Limjap  
June 6, 2016  

## Import libraries to be used in this analysis

```r
library(dplyr)
library(lubridate)
library(ggplot2)
```

## Loading and preprocessing the data.


Data is saved and extracted into the current working directory in the folder /data 

```r
if(!file.exists("./data")){dir.create("./data")}
fileURL <- "https://d396qusza40orc.cloudfront.net/repdata%2Fdata%2Factivity.zip"
download.file(fileURL,destfile="./data/repdata-data-activity.zip",method="curl")
unzip ("./data/repdata-data-activity.zip", exdir = "./data")
```


Read in the csv file in the /data directory

```r
file_path <- "./data/activity.csv"
data_full <- read.csv(file_path, header=TRUE, sep=',', na.strings="", 
             check.names=FALSE, stringsAsFactors=FALSE, comment.char="", quote='\"')
```


Convert the data frame columns of the `data_full` dataframe to data formats appropriate for analysis
`data_full`'s columns are converted using the `tbl_df` of the `dplyr` library and `ymd` of the `lubridate` library

```r
data_full <- tbl_df(data_full)
data_full$date <- ymd(data_full$date)
data_full$steps <- as.numeric(data_full$steps)
```

```
## Warning: NAs introduced by coercion
```

Check dataframe structure

```r
nrow(data_full)
```

```
## [1] 17568
```

```r
glimpse(data_full)
```

```
## Observations: 17,568
## Variables: 3
## $ steps    (dbl) NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N...
## $ date     (date) 2012-10-01, 2012-10-01, 2012-10-01, 2012-10-01, 2012...
## $ interval (int) 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 100, 10...
```

## What is mean total number of steps taken per day?
Create a subset for `data_full` group by date, remove NA values and get the total steps for each group

```r
total_steps_day <- data_full %>% group_by(date) %>%
    filter(!is.na(steps)) %>%
    summarise(total_steps= sum(steps))
```


Barplot shows the total steps per day

```r
ggplot(total_steps_day, aes(date, total_steps)) + geom_bar(stat = "identity", fill= "firebrick") +
    xlab("Date") + ylab("Total") + ggtitle("Total Steps per Day")
```

![](PA1_template_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

```r
ggsave("./figure/plot1.png")
```

```
## Saving 7 x 5 in image
```

Histogram of total steps

```r
ggplot(total_steps_day, aes(total_steps)) + geom_histogram(fill= "firebrick") +
    labs(title = "Daily Steps", x = "Total Steps", y = "Frequency")
```

```
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

![](PA1_template_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

```r
ggsave("./figure/plot2.png")
```

```
## Saving 7 x 5 in image
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

`mean` and `median` of total steps per day

```r
mean(total_steps_day$total_steps)
```

```
## [1] 10766.19
```

```r
median(total_steps_day$total_steps)
```

```
## [1] 10765
```

### What is the average daily activity pattern?
Make a time series plot (i.e. 𝚝𝚢𝚙𝚎 = "𝚕") of the 5-minute interval (x-axis) and the average number of steps taken, averaged across all days (y-axis)
Subset by interval then get the average of steps per iterval

```r
daily_act_pattern <- data_full %>% group_by(interval) %>% 
    filter(!is.na(steps)) %>%
    summarise(ave_steps = mean(steps))
```

Plotting the results

```r
ggplot(daily_act_pattern, aes(interval, ave_steps)) + 
    geom_line(linetype= 1, color= "firebrick") +
    labs(title = "Average steps per Interval", x = "Interval", y = "Steps")
```

![](PA1_template_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

```r
ggsave("./figure/plot3.png")
```

```
## Saving 7 x 5 in image
```

Which 5-minute interval, on average across all the days in the dataset, contains the maximum number of steps?

```r
daily_act_pattern[which.max(daily_act_pattern$ave_steps), ]
```

```
## Source: local data frame [1 x 2]
## 
##   interval ave_steps
##      (int)     (dbl)
## 1      835  206.1698
```

### Inputing missing values
Calculate and report the total number of missing values in the dataset. (i.e. the total number of rows with NAs)

```r
sum(is.na(data_full))
```

```
## [1] 2304
```

Devise a strategy for filling in all of the missing values in the dataset.
Create a new dataset that is equal to the original dataset but with the missing data filled in.

```r
data_fill_na <- data_full
nas <- is.na(data_full$steps)
avg_interval <- tapply(data_fill_na$steps, data_fill_na$interval, mean, na.rm=TRUE, simplify=TRUE)
data_fill_na$steps[nas] <- avg_interval[as.character(data_fill_na$interval[nas])]
```

Verify that there are no missing values

```r
sum(is.na(data_fill_na$steps))
```

```
## [1] 0
```

Make a histogram of the total number of steps taken each day and Calculate and report the mean and median total number of steps taken per day. 
Do these values differ from the estimates from the first part of the assignment? 
What is the impact of imputing missing data on the estimates of the total daily number of steps?


```r
steps_full <- data_fill_na %>%
    filter(!is.na(steps)) %>%
    group_by(date) %>%
    summarize(steps = sum(steps)) %>%
    print
```

```
## Source: local data frame [61 x 2]
## 
##          date    steps
##        (date)    (dbl)
## 1  2012-10-01 10766.19
## 2  2012-10-02   126.00
## 3  2012-10-03 11352.00
## 4  2012-10-04 12116.00
## 5  2012-10-05 13294.00
## 6  2012-10-06 15420.00
## 7  2012-10-07 11015.00
## 8  2012-10-08 10766.19
## 9  2012-10-09 12811.00
## 10 2012-10-10  9900.00
## ..        ...      ...
```



```r
ggplot(steps_full, aes(steps)) +
    geom_histogram(fill = "firebrick", binwidth = 1000) +
    labs(title = "Histogram of Steps per day, fixed missing values", x = "Steps", y = "Frequency")
```

![](PA1_template_files/figure-html/unnamed-chunk-18-1.png)<!-- -->

```r
ggsave("./figure/plot4.png")
```

```
## Saving 7 x 5 in image
```

The `mean` and `median` for the new dataframe with NA values filled in.

```r
mean(steps_full$steps)
```

```
## [1] 10766.19
```

```r
median(steps_full$steps)
```

```
## [1] 10766.19
```

### Are there differences in activity patterns between weekdays and weekends?
Create a new factor variable in the dataset with two levels – “weekday” and “weekend” indicating whether a given date is a weekday or weekend day.

```r
data_fill_na <- mutate(data_fill_na, 
        weektype = ifelse(weekdays(data_fill_na$date) == "Saturday" | 
        weekdays(data_fill_na$date) == "Sunday", "weekend", "weekday"))
data_fill_na$weektype <- as.factor(data_fill_na$weektype)
head(data_fill_na)
```

```
## Source: local data frame [6 x 4]
## 
##       steps       date interval weektype
##       (dbl)     (date)    (int)   (fctr)
## 1 1.7169811 2012-10-01        0  weekday
## 2 0.3396226 2012-10-01        5  weekday
## 3 0.1320755 2012-10-01       10  weekday
## 4 0.1509434 2012-10-01       15  weekday
## 5 0.0754717 2012-10-01       20  weekday
## 6 2.0943396 2012-10-01       25  weekday
```
Make a panel plot containing a time series plot (i.e. 𝚝𝚢𝚙𝚎 = "𝚕") of the 5-minute interval (x-axis) and the average number of steps taken, averaged across all weekday days or weekend days (y-axis).

```r
interval_full <- data_fill_na %>%
    group_by(interval, weektype) %>%
    summarise(steps = mean(steps))
```


```r
s <- ggplot(interval_full, aes(interval, steps, colour = weektype)) +
    geom_line() +
    scale_colour_discrete(name= "Type", labels= c("Week day", "Weekend")) +
    facet_wrap(~weektype, ncol = 1, nrow=2)
print(s)
```

![](PA1_template_files/figure-html/unnamed-chunk-22-1.png)<!-- -->

```r
ggsave("./figure/plot5.png")
```

```
## Saving 7 x 5 in image
```

The average number of steps is higher on weekends at interval point 1000- 2000
